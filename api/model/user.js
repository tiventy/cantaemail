const mongoose = require('mongoose');
const userSchema = mongoose.Schema({
    //_id: mongoose.Schema.Types.ObjectId,
    email:{type: String, required: true, unique: true,
    },
    created: {
        type: Date,
        default: Date.now
    },
});

module.exports = mongoose.model('Users', userSchema);