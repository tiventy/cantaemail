const Users = require('../model/user');
var mongoose = require('mongoose');
var nodemailer = require('nodemailer');


exports.user_post = (req, res) => {
    if (req.file)
        console.log(req.file);
    Users.find({email: req.body.email}).exec().then(user => {
        if (user.length >= 1) {
            return res.status(200).json({
                ResponseCode: "422",
                message: "email exist"
            });
        }
        else {
                const user = new Users({
                    _id: new mongoose.Types.ObjectId(),
                    email: req.body.email,
                });
                user
                    .save()
                    .then(result => {
                        var transporter = nodemailer.createTransport({
                            service: 'gmail',
                            auth: {
                                user: 'tiventyglobal@gmail.com',
                                pass: '07032155156',
                            }
                        });
                        const mailOptions = {
                            from: 'hello@tiventyglobal.com', // sender address
                            to: user.email, // list of receivers
                            subject: ' Canta Services', // Subject line
                            //text: 'Hello ' + ', thank you for registering at localhost.com. Please click on the following link to complete your activation: '+req.hostUrl+'/user/email' ,
                            html: 'Hello<strong> ' + '</strong>,<br><br>Thank you for subscribing to canta.io .<br><br><a href="'+req.hostUrl + '/user/email' + '">Click here</a>'
                        };
                        transporter.sendMail(mailOptions, function (err, info) {
                            if (err)
                                console.log(err)
                            else
                                console.log(info);
                        });
                        console.log(req.body.email);

                        console.log(result);
                        res.status(200).json({ResponseCode: "200", message: "Email saved", createduser: user});
                    })
                    .catch(err => res.status(500).json({ResponseCode: "500", error: err, message: "registration not successful"}));
            }
        })

};
exports.user_get_all = (req, res, next) => {
    var mysort = { created: -1 };

    Users.find().sort(mysort)
        .select('user _id email created')
        .exec()
        .then(docs => {
            res.status(200).json(
                {
                    ResponseCode: "200",
                    count: 100 + docs.length,
                    user: docs
                })
        }).catch(err => {
        res.status(500).json({
            ResponseCode: "500",
            message: err
        });
    });

};
