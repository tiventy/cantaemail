require('dotenv').config();
var http = require('http');

var app = require('./app');


var port = process.env.PORT || 9000;

var server = http.createServer(app);


server.listen(port);

console.log(`App running on port ${port}`);