
const logger = require('./logger');
const express = require('express');
var cors = require('cors')


const app = express();
app.logger = logger;

const morgan = require('morgan');
const bodyparser = require('body-parser');
const  mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

//var mongoose = require('mongoose');
var url = 'mongodb://localhost:27017/CantaSubscribers';

mongoose.connect(url);

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.on('open', function () {
    console.log('connected');

});


//mongoose.connect('mongodb+srv://sammy:' + process.env.MONGO_ATLAS_PW + '@tiventy-api-mc4p2.mongodb.net/test?retryWrites=true',{ useNewUrlParser: true } );


var userRoutes = require('./api/routes/user');


app.use(cors());

app.use(morgan('dev'));
app.use((req,_,next)=>{ //loads all necessary details
    req.hostUrl = `${req.headers['x-forwarded-proto'] || req.protocol}://${req.get('host')}`;
    try {
        req.Userdata = jwt.verify(req.headers.authorization.split(" ")[1], process.env.JWT_TOKEN);
    } catch (error) {  }
    next();
});

app.use('/logs',express.static(app.logger.logPath),require('serve-index')(app.logger.logPath, { icons: true }));
//  app.use((req, res, next) => {
//   res.header('Access-Control-Allow-Origin', '*');
//    res.header('Access-Control-Allow-Headers', "Origin, X-Requested-Width, Content-Type,Authorization");
//
// if (req.method === 'OPTIONS') {
//     res.header('Access-Control-Allow-Methods', 'PUT, POST, DELETE, PATCH');
//      return res.status(200).json({});
//  }
// });


//app.use('/uploadedevent',express.static('uploadedevent'));
app.use(bodyparser.urlencoded({extended: false}));
app.use(bodyparser.json());



app.use('/user', userRoutes);

app.all('*',(req, res) =>
{
    console.log(`Not found ${req.url}`);
    res.status(404).end('Not found');
});

app.use((error, req, res) =>
{
    console.log(error);
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});

module.exports = app;
